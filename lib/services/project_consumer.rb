module Services
  class ProjectConsumer
    require 'httparty'
    include HTTParty

    class << self
      def update_active_causes
        api_key_param = {:api_key => SiteConfig.global_giving_api_key}
        response = get("#{SiteConfig.global_giving_host}#{SiteConfig.global_giving_project_id_end_point}", :query => api_key_param)
        recent_ids = response["projects"]["project"].collect{|projects_hash| projects_hash["id"]}

        existing_ids = Cause.active.pluck(:project_identifier).collect(&:to_s)

        latest_project_ids = recent_ids - existing_ids
        expired_cause_identifiers = existing_ids - recent_ids


        #Expire outdated causes
        causes = Cause.for_project_identifiers(expired_cause_identifiers)
        unless causes.blank?
          puts "--- Deactivating #{causes.size} causes"
          puts "Expired project identifers: #{expired_cause_identifiers.inspect}"
          ActiveRecord::Base.transaction do
            Cause.update_all('active = false', {:id => causes.collect(&:id)})
          end
        end

        #Load the new causes
        puts "--- Adding #{latest_project_ids.size} causes"
        puts "New project identifers: #{latest_project_ids.inspect}"
        latest_project_ids.each_slice(10) do |project_id_slice|
          project_response = get("#{SiteConfig.global_giving_host}#{SiteConfig.global_giving_project_details}", :query => api_key_param.merge(:projectIds => project_id_slice.join(",")))
          puts project_response.inspect
          Cause.create_from_hash(project_response)
        end
      end
    end #end of the self block
  end
end
