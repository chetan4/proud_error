namespace :script do
  desc "Task to fetch the active projects from global giving and activate/deactivate the existing causes in the database"
  task :update_active_causes => :environment do
    Services::ProjectConsumer.update_active_causes
  end

  desc "Task to generate tweet message with hashtags"
  task :generate_tweets => :environment do
    include Rails.application.routes.url_helpers # brings ActionDispatch::Routing::UrlFor
    include ActionView::Helpers::TagHelper
    host = "http://prouderrors.org"
    urls = []
    Cause.active.each do |cause|
      cause_title = "#{cause.title} - "
      url = host + cause_path(cause.theme.parameterize, cause.title.parameterize, cause.project_identifier)
      twitter_handle = " @prouderrors"
      hash_tags = cause.theme.split.collect(&:downcase).reject{|str| "and" == str || "&" == str}.collect{|str| "##{str}"}
      puts((cause_title + url + twitter_handle + " " + hash_tags.join(" ")))
    end
  end
end