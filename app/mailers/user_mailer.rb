class UserMailer < ActionMailer::Base
  #default from: "from@example.com"
  def contact_us(name,email,message)
    @name = name
    @email = email
    @message = message
    mail(:to=>'jparekh@idyllic-software.com',:subject=>"ProudError Feedback",from: @email)
  end
end
