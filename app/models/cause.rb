class Cause < ActiveRecord::Base
  attr_accessible :project_identifier, :summary, :small_image_url, :medium_image_url, 
    :large_image_url, :project_url, :theme, :title

  validates :project_identifier, :presence => true # project identifiers are not always unique. 
  validates :summary, :presence => true
  validates :project_url, :presence => true
  validates :theme, :presence => true
  validates :title, :presence => true

  scope :for_project_identifiers, lambda{|project_ids| where(:project_identifier => project_ids)}

  scope :active, where(:active => true)
  
  scope :random, active.order("RAND()").limit(1) #This is mysql db specific scope. For postgres one can use the RANDOM() function.

  scope :unique_themes, select(:theme).uniq.order("causes.theme ASC")

  scope :for_ids, lambda{|cause_ids|where(:id => cause_ids)}

  ALL_THEMES = "All Themes"

  class << self
    def cached_themes(force=false)
      cache_key = "causes_unique_themes"
      Rails.cache.fetch(cache_key, :force => force, :expires_in =>  SiteConfig.ttls.unique_themes) do
        themes = []
        unique_themes.each do |uniq_theme|
          themes << uniq_theme.attributes
        end
        themes
      end
    end

    def cached_theme_names
      cached_themes.collect{|theme_name| theme_name['theme']}
    end

    def create_from_hash(project_hash)
      projects = project_hash["projects"]["project"]
      projects = [projects] if projects.is_a?(Hash)
      projects.each do |project|
        if project['title'].present?
          small_link_url = nil
          medium_link_url = nil
          large_link_url = nil

          project['image']['imagelink'].each do |imagelink|
            small_link_url = imagelink["url"] if "small" == imagelink["size"].to_s.downcase
            medium_link_url = imagelink["url"] if "medium" == imagelink["size"].to_s.downcase
            large_link_url = imagelink["url"] if "large" == imagelink["size"].to_s.downcase
          end

          ActiveRecord::Base.transaction do

            Cause.create!(:project_identifier => project['id'],
              :summary => project['summary'],
              :small_image_url => small_link_url,
              :medium_image_url => medium_link_url,
              :large_image_url => large_link_url,
              :project_url => project['projectLink'],
              :theme => project['themeName'],
              :title => project['title'])
          end
        end
      end
    end
    
    def cause_ids_for_a_theme(theme_name)
      grouped_cause_ids[underscored(theme_name)]
    end
    
    def fetch(partner)
      partner.has_theme? ? fetch_for_theme(partner.theme) : fetch_random
    end

    def for_a_theme(parameterized_theme)
      cause_ids = cause_ids_for_a_theme(underscored(parameterized_theme))
      puts "=== #{cause_ids}"
      Cause.for_ids(cause_ids)
    end
    

    private
    def fetch_for_theme(theme_name)
      #Fetch a random cause but for a theme
      cause_ids = cause_ids_for_a_theme(theme_name)

      cause_ids.present? ? for_ids(cause_ids).random.first : fetch_random
    end
    
    def fetch_random
      random.first
    end


    def grouped_cause_ids(force=false)
      #methods creates a hash for all theme names in the form: {"climate_change"
      #=> [1, 2, 3....], "animals" => [4, 5, 6...]} and
      # puts it away in the cache
      cache_key = "grouped_theme_cause_ids"
      Rails.cache.fetch(cache_key, :force => force, :expires_in =>  SiteConfig.ttls.grouped_theme_causes) do
        themes_group = {}
        cached_theme_names.each{|theme_name| themes_group[underscored(theme_name)] = []}
        
        active.each do |cause|
          themes_group[underscored(cause.theme)] << cause.id
        end
        themes_group
      end
    end

    def underscored(theme_name)
      theme_name.to_s.parameterize.underscore
    end
  end #end of the self block
end