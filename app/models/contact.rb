class Contact
  
  include ActiveModel::Validations
  include ActiveModel::Serializers
  include ActiveModel::MassAssignmentSecurity
  include ActiveModel::Conversion
  include ActiveModel::Naming

  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  attr_accessor :name, :message, :email

  validates :name,:presence => true
  validates :message,:presence => true
  validates :email,:presence => true,:format => EMAIL_REGEX

  def initialize(options={})
    @name = options[:name]
    @email = options[:email]
    @message = options[:message]
  end
 
  def deliver_contact_us
     UserMailer.contact_us(name,email,message).deliver if self.valid?
  end

  # OVERIDDEN METHOD FOR ACTIVE MODEL TO NOT ACT AS A ACTIVERECORD MODEL
  def persisted?
    false
  end
end