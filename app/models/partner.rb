class Partner < ActiveRecord::Base
  attr_accessible :url, :code, :theme

  VALID_URL_REGEX_SET = /^(https?:\/\/(w{3}\.)?)|(w{3}\.)|[a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(?:(?::[0-9]{1,5})?\/[^\s]*)?/ix
  validates :url, :uniqueness => true, :format => { :with => VALID_URL_REGEX_SET }
  validates :code, :presence => true, :uniqueness => true

  scope :order_descended_creation, order("partners.created_at DESC")

  scope :active, where(:active => true)

  before_validation :sanitize_url, :assign_code

  class << self
    def ordered_active
      active.order_descended_creation
    end
    
    def set_url(url)
      url.gsub(/^(https?:\/\/(w{3}\.)?)| w{3}\./ix,"")
    end
  end #end of the self block

  def set_theme(theme_name)
    if theme_name.present? && Cause::ALL_THEMES.downcase != theme_name.to_s.strip.downcase
      self.theme = theme_name
      self.save
    end
  end

  def has_theme?
    self.theme.present?
  end

  private
  def sanitize_url
    self.url = self.url.to_s.gsub(/^(https?:\/\/(w{3}\.)?)| w{3}\./ix, "")
  end
  
  def assign_code
    self.code = UUIDTools::UUID.random_create.to_s.gsub(/[-]/, "")
  end
end