module ApplicationHelper
  def title_content
    content_for(:title) do
      if defined?(@title) && @title.present?
        return @title
      end
      "Proud Errors"
    end
  end

  def complete_page_replacement_script
    script = "jQuery(document).ready(function(){"
    script << "jQuery('body').html('');"
    script << "jQuery.getJSON('http://#{SiteConfig.app_base_url}#{display_full_cause_path(@partner.code, :format => :json)}?jsoncallback=?', null, function(data){"
    script << "});"
    script << "});"
    script.html_safe
  end

  def partial_page_replacement_script
    script = "jQuery(document).ready(function(){"
    script << "jQuery.getJSON('http://#{SiteConfig.app_base_url}#{display_section_cause_path(@partner.code, :format => :json)}?jsoncallback=?', null, function(data){"
    script << "});"
    script << "});"
    script.html_safe
  end
end