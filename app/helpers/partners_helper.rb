module PartnersHelper
  def tableize_partners(partners)
    partners_table = []
    unless partners.blank?
      add_blank_partners(partners)

      partners.each_slice(3) do |partners_slice|
        partners_table << partners_slice
      end

      list1 = []; list2 = []; list3 = []

      partners_table.each do |partners_row|
        list1 << partners_row[0]
        list2 << partners_row[1]
        list3 << partners_row[2]
      end

      [list1, list2, list3]
    end
  end

  def add_blank_partners(partners)
    while 0 != (partners.length % 3)
      partners << Partner.new()
    end
  end
end
