module CausesHelper
  def active_class(theme_hash)
    params['theme'] == theme_hash['theme'].parameterize ? "active" : ""
  end
end
