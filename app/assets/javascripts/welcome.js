$(document).ready(function(){
   var welcome = new Welcome();
   welcome.initialize();
});

function Welcome(){
    var $getStarted = $('.getStartedLink')

    this.initialize = function(){
        if($getStarted.length){
            this.hookGetStarted();
            this.hookGoTop();
        }
    };

    this.hookGetStarted = function(){
        $('body').on('click', '.getStartedLink', function(){
            if($('#partner_form_container').is(':visible')){
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }

            $('#introduction_container').animate({
                opacity: 'toggle',
                width: 'toggle'
            }, 500, function(){
                $('#partner_form_container').animate({
                    opacity: 'toggle',
                    width: 'toggle'
                },  500);
            });

            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
    };

    this.hookGoTop = function(){
        $('body').on('click', '.goTop', function(){
            $("html, body").animate({ scrollTop: 0 }, 1000);
        });
    };
}