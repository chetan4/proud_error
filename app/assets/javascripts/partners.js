$(document).ready(function(){
    var partner = new Partner();
    partner.initialize();
});

function Partner(){
    var $partnersForm = $('#partner_form');
    var current = this;
    
    this.initialize = function(){
        if($partnersForm.length){
            this.hideComponents();
            this.hookLinkBasedSubmission();
            this.hookFormSubmission();
            this.hookCopyToCipboard();
            this.hookScrollToHow();
        }
    };

    this.hideComponents = function(){
        $('.loading').hide();
        $('#partner_form_container').hide();
        $('.embedcode').hide();
    };

    this.hookLinkBasedSubmission = function(){
        $('body').on('click', '.registerLink', function(){
            $partnersForm.submit();
        });
    };

    this.hookFormSubmission = function(){
        $('body').on('submit', '#partner_form', function(){
            $.ajax({
                type: "POST",
                url: $partnersForm.attr('action'),
                data: $partnersForm.serializeArray(),
                beforeSave: function(){
                    $('.registerLink').hide();
                    $('.loading').show();
                },
                error: function(data){
                    var  parsedData = $.parseJSON(data.responseText);
                    $('#partner_page_uuid').html("<srtrong>Error!!!</strong>");
                    $('#partner_section_uuid').html("<srtrong>Error!!!</strong>");
                    $("#partner_url").next('br').remove();
                    $("#partner_url").next('.inline-helper').remove();
                    $("#partner_url").after('<br /><span class="inline-helper">' + parsedData['url'] + '</span>')
                },
                success: function(data){
                    var sectionScriptSrc = "http://" + $('#app_base_url').val() + "/cause.js?code=" + data['code'];
                    var partialScriptSrc = sectionScriptSrc + "&section=partial";

                    
                    var firstLine = '<div style="margin:0;padding:0;display:inline">';
                    var completePageScriptscript = current.createEmbedCode(firstLine, sectionScriptSrc);
                    $('#partner_page_uuid').text(completePageScriptscript);

                    firstLine = '<!-- Customize the look and feel of the cause by modifying the CSS below -->' +
                    '<style type="text/css">' +
                    '#proudErrorContainer{width: 600px; border: 1px solid grey; font-family: Arial; margin: 10px auto; font-size: 12px;}' +
                    '#proudErrorContainer .title{color: #D6344E; font-size: 16px; font-style: italic; font-weight: bold; margin: 0 0 10px; text-align: left;}' +
                    '#proudErrorContainer .summary{font-style: italic; bold; margin: 0 0 10px; text-align: left;}' +
                    '</style>' +
                    '<div id="proudErrorContainer">';
                    var partialSection = current.createEmbedCode(firstLine, partialScriptSrc);
                    $('#partner_section_uuid').text(partialSection);

                    $('#partner_form_container').animate({
                        opacity: 'toggle',
                        width: 'toggle'
                    }, 500, function(){
                        $('.embedcode').animate({
                            opacity: 'toggle',
                            width: 'toggle'
                        }, 500);
                    });
                }
            });

            return false;
        });
    };

    this.hookCopyToCipboard = function(){
        $('body').on('click', '.copyToClipboard', function(){
            $(this).zclip({
                path:'assets/zclip/ZeroClipboard.swf',
                copy: $(this).parents('.embedcodeContainer').first().find('.iframeCode').text(),
                afterCopy: function(){
                    return null;
                }
            });
        });
    };

    this.hookScrollToHow = function(){
        $('body').on('click', '.scrollToHow', function(){
            $('html, body').animate({
                scrollTop: $("#howitworks").offset().top
                }, 2000);
        });
    };

    this.createEmbedCode = function(containerFirstLine, customScript){
        return  containerFirstLine +
        "<script type='text/javascript' src='http://" + $('#app_base_url').val() + "/cause_assets/jquery.js'></script>" +
        "<script type='text/javascript' src='" + customScript + "'></script>" +
        "</div>"
    };
}