class WelcomeController < ApplicationController
  before_filter :set_title

  def index
#    @participating_count = Partner.active.count
    @theme_names = get_theme_names
  end

  private
  def get_theme_names
    cached_themes = Cause.cached_theme_names
  end

  def set_title
    @title = "Recycle 404 Error Pages"
  end
end
