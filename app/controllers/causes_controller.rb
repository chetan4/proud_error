class CausesController < ApplicationController
  before_filter :themes_hash
  before_filter :populate_display_vars, :only => [:display_full, :display_section]

  def index
    redirect_to list_theme_causes_path(@themes.first['theme'].parameterize)
  end

  def list_theme
    @causes = Cause.for_a_theme(params[:theme])
    @title = "#{params[:theme].titleize} - Proud Errors"
  end

  def show
    @cause = Cause.find_by_project_identifier(params[:project_identifier])
    @title = "#{@cause.title}"
  end

  def display_full
    
    respond_to do |format|
      format.html {render :layout => "proud_error"}

      #code to fetch the action view html
      av = action_view
      av.assign({:partner => @partner, :cause => @cause})
      format.json {render :text => "jQuery('body').html('#{av.render(:file => "causes/display_full.html.erb", :layout => "layouts/proud_error.html.erb", :handlers => [:erb]).gsub(/\n/, "")}')"}
    end
  end

  def display_section
    respond_to do |format|
      format.html {render :layout => "proud_error"}

      #code to fetch the action view html
      av = action_view
      av.assign({:partner => @partner, :cause => @cause})
      format.json {render :text => "jQuery('#proudErrorContainer').html('#{av.render(:file => "causes/display_section.html.erb", :layout => "layouts/blank.html.erb", :handlers => [:erb]).gsub(/\n/, "")}')"}
    end
  end

  private
  def populate_display_vars
    @partner = Partner.find_by_code(params[:uuid])
    @partner.update_attribute(:active, true)
    @cause = Cause.fetch(@partner)
  end

  def themes_hash
    @themes = Cause.cached_themes
  end

  def action_view
    controller = ActionController::Base.new
    controller.request = ActionDispatch::TestRequest.new
    TaskActionView.new(Rails.root.join('app', 'views'), {}, controller)
  end

  class TaskActionView < ActionView::Base
    include Rails.application.routes.url_helpers
    include ApplicationHelper

    def default_url_options
      {:host =>  SiteConfig.app_base_url}
    end

    def protect_against_forgery?
      false
    end
  end
end
