class PartnersController < ApplicationController
  include Rails.application.routes.url_helpers # brings ActionDispatch::Routing::UrlFor
  include ActionView::Helpers::TagHelper
  
  before_filter :fetch_partner, :check_partner_exists,:only => [:create]

  def list
    @partners = Partner.ordered_active
  end

  def new
    @partner = Partner.new
	end

	def create
    @partner = Partner.new(params[:partner])
    respond_to do |format|
      if @partner.save
        format.html {render :text => cause_full_path}
          format.json {render :json => {:src => cause_full_path, :code => @partner.code}.to_json}
      else
        status = :unprocessable_entity
        format.html {render :text => @partner.errors.full_messages, :status => status}
        format.json {render :json => {:url => @partner.errors.full_messages.first.gsub(/Url/, "URL")}.to_json, :status => status}
      end
    end
	end

  def cause_js
    code = params[:code]
    @partner = Partner.find_by_code(code)
    respond_to do |format|
      format.html {response.headers['Content-Type'] = "application/javascript";render :partial => "partners/cause.js"}
      format.js {response.headers['Content-Type'] = "application/javascript";render :partial => "partners/cause.js"}
    end
  end

  private
  def check_partner_exists
    if fetch_partner.present?
      set_theme
      respond_to do |format|
        format.html {render :text => cause_full_path}
        format.json {render :json => {:src => cause_full_path, :code => @partner.code}.to_json}
      end
      false
    end
  end

  def set_theme
    fetch_partner.set_theme(params[:partner][:theme])
  end
  
  def fetch_partner
    @partner ||= Partner.find_by_url(Partner.set_url(params[:partner][:url]))
  end

  def cause_full_path
    "http://#{SiteConfig.app_base_url}#{cause_partner_path(@partner.code)}"
  end
  
  def fetch_template_code
    template = Template.new(Rails.root.join('app', 'views'))
    template.assign(:partner =>  @partner, :cause => @cause)
    template.render(:template => 'partners/cause.html.erb')
  end
end
