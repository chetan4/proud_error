class ContactsController < ApplicationController
  
  def create
    @contact = Contact.new(:name => params[:name], :email => params[:email], :message => params[:message])
    if @contact.valid?
      @contact.deliver_contact_us
      render :text => "success", :layout => false
    else
      render :action => "new"
    end
  end
end
