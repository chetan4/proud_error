class AddTitleToCauses < ActiveRecord::Migration
  def change
    add_column(:causes, :title, :string, :limit => 755)
  end
end
