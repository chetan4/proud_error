class AddThemeToCauses < ActiveRecord::Migration
  def change
    add_column :causes, :theme, :string, :limit => 755
  end
end
