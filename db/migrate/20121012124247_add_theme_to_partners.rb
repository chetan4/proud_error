class AddThemeToPartners < ActiveRecord::Migration
  def change
    add_column(:partners, :theme, :string, :limit => 755)
  end
end
