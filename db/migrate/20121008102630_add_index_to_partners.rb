class AddIndexToPartners < ActiveRecord::Migration
  def change
    add_index(:partners, :code, :unique => true)
  end
end
