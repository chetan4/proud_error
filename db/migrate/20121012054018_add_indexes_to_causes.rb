class AddIndexesToCauses < ActiveRecord::Migration
  def change
    add_index(:causes, [:active, :created_at])
    add_index(:causes, [:project_identifier])
  end
end
