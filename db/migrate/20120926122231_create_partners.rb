class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :url
      t.string :code
      t.timestamps
    end
  end
end