class AddActiveFlagToCauses < ActiveRecord::Migration
  def change
    add_column :causes, :active, :boolean, :default => true
  end
end
