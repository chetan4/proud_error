class CreateCauses < ActiveRecord::Migration
  def change
    create_table :causes do |t|
      t.integer :project_identifier
      t.text :summary
      t.string :small_image_url, :limit => 2303
      t.string :medium_image_url, :limit => 2303
      t.string :large_image_url, :limit => 2303
      t.string :project_url, :limit => 2303

      t.timestamps
    end
  end
end
