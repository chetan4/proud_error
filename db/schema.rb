# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121012124247) do

  create_table "causes", :force => true do |t|
    t.integer  "project_identifier"
    t.text     "summary"
    t.string   "small_image_url",    :limit => 2303
    t.string   "medium_image_url",   :limit => 2303
    t.string   "large_image_url",    :limit => 2303
    t.string   "project_url",        :limit => 2303
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.boolean  "active",                             :default => true
    t.string   "theme",              :limit => 755
    t.string   "title",              :limit => 755
  end

  add_index "causes", ["active", "created_at"], :name => "index_causes_on_active_and_created_at"
  add_index "causes", ["project_identifier"], :name => "index_causes_on_project_identifier"

  create_table "partners", :force => true do |t|
    t.string   "url"
    t.string   "code"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.boolean  "active",                    :default => false
    t.string   "theme",      :limit => 755
  end

  add_index "partners", ["code"], :name => "index_partners_on_code", :unique => true

end
