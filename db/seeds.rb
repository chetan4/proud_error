# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Seed the causes
dirpath = (Rails.root.to_s + "/db/cause_seed_xmls/")
Dir.foreach(dirpath) do |filename|
  if "." != filename && ".." != filename && 'insert_stmt.sql' != filename
    puts "Parsing file: #{(dirpath + filename)}"
    File.open((dirpath + filename), "r") do |file|
      file_content = file.read
      project_hash = Hash.from_xml(file_content)
      Cause.create_from_hash(project_hash)
    end
  end
end