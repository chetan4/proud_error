ProudErrorRails::Application.routes.draw do
  match "/cause.js", :to => "partners#cause_js", :as => "cause_js_partners", :via => :get
  root :to => 'welcome#index'

  resources :contacts, :only => [:create]
  get "/partners", :to => "partners#new", :as => "index_partner"
  resources :partners do
    member do
      get 'cause'
    end

    collection do
      get 'list'
    end
  end

  # named routes for cause listing, individual cause etc
  match "/causes", :to => "causes#index", :as => "causes"
  match "/causes/:theme", :to => "causes#list_theme", :as => "list_theme_causes"
  match "/causes/:theme/show/:title/:project_identifier", :to => "causes#show", :as => "cause"

  #named routes for displaying full or partial causes
  match "/causes/:uuid/display/full", :to => "causes#display_full", :as => "display_full_cause"
  match "/causes/:uuid/display/section", :to => "causes#display_section", :as => "display_section_cause"
end