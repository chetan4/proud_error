# Load the rails application
require File.expand_path('../application', __FILE__)
require 'services/project_consumer'

# Initialize the rails application
ProudErrorRails::Application.initialize!
