require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'http://www.prouderrors.org'
SitemapGenerator::Sitemap.create do
  add '/', :changefreq => 'daily', :priority => 0.9
  add '/partners/list', :changefreq => 'daily', :priority => 0.9
  add '/partners/list', :changefreq => 'daily', :priority => 0.9
  add '/causes', :changefreq => 'weekly', :priority => 0.9

  #Add the links for themes
  Cause.cached_themes.each do |theme_name|
    add "/causes/#{theme_name['theme'].parameterize}", :changefreq => 'weekly', :priority => 0.9
  end

  Cause.active.each do |cause|
    add "/causes/#{cause.theme.parameterize}/show/#{cause.title.parameterize}/#{cause.project_identifier}", :changefreq => 'weekly', :priority => 0.9
  end
end
SitemapGenerator::Sitemap.ping_search_engines # called for you when you use the rake task
